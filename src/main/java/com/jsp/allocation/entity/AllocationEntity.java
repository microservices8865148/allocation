package com.jsp.allocation.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="allocation_entity")
public class AllocationEntity implements Serializable {

	@Id
	@Column(name="allocation_id")
	@GenericGenerator(name="auto-reg",strategy="increment")
	@GeneratedValue(generator="auto-reg")
	private long allocationId;
	
	@Column(name="allocation_number")
	private int allocationNumber;
	
	@Column(name="allocation_date")
	private LocalDate allocationDate;
	
	@Column(name="allocation_year")
	private int allocationYear;
	
	@Column(name="status")
	private String status;
	
	@Column(name="grant_id")
	private long grantId;
	
	@Column(name="created_date")
	private LocalDate createdDate;

	public long getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(long allocationId) {
		this.allocationId = allocationId;
	}

	public int getAllocationNumber() {
		return allocationNumber;
	}

	public void setAllocationNumber(int allocationNumber) {
		this.allocationNumber = allocationNumber;
	}

	public LocalDate getAllocationDate() {
		return allocationDate;
	}

	public void setAllocationDate(LocalDate allocationDate) {
		this.allocationDate = allocationDate;
	}

	public int getAllocationYear() {
		return allocationYear;
	}

	public void setAllocationYear(int allocationYear) {
		this.allocationYear = allocationYear;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getGrantId() {
		return grantId;
	}

	public void setGrantId(long grantId) {
		this.grantId = grantId;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}
	
	
	
	
}
