package com.jsp.allocation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jsp.allocation.entity.AllocationEntity;

public interface AllocationRepository extends JpaRepository<AllocationEntity,Long>{

}
