package com.jsp.allocation.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface AllocationService{
	
	public String processGetPendingAllocationStatus(String li)throws JsonMappingException, JsonProcessingException;
	public void processUpdateAllocationStatus(List<Long> ids) ;
}
