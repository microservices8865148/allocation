package com.jsp.allocation.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsp.allocation.entity.AllocationEntity;
import com.jsp.allocation.repository.AllocationRepository;

@Service
public class AllocationServiceImpl implements AllocationService {

	@Autowired
	private WebClient webClient;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private AllocationRepository allocationRepository;
	
	
	
	@Override
//	@Scheduled(cron="* */5 * * * *")
	@KafkaListener(topics={"list"},groupId="allocation")
	public String processGetPendingAllocationStatus(String li) throws JsonMappingException, JsonProcessingException {
//		String string=webClient.get().uri("/gs/pendingAllocationStatus/pending").retrieve().bodyToMono(String.class).block(); 
		List<Map<String, Object>> readValue = objectMapper.readValue(li, new TypeReference<List<Map<String,Object>>>() {});
		int count=0;
		List<AllocationEntity> list = new ArrayList<AllocationEntity>();
		List<Long> longs = new ArrayList<Long>();
		for (Map<String,Object> map: readValue) {
			
			for(int i=0;i<Integer.parseInt( map.get("frequency").toString());i++) {
				AllocationEntity allocationEntity = new AllocationEntity();
				allocationEntity.setAllocationNumber((Integer.parseInt(map.get("numberOfGrants").toString()))/(Integer.parseInt(map.get("frequency").toString())));
				allocationEntity.setStatus(map.get("allocationStatus").toString());
//				System.out.println(map.get("grantDate").toString());
//				allocationEntity.setAllocationDate(LocalDate.parse(map.get("grantDate").toString(),DateTimeFormatter.ofPattern("[yyyy,MM,dd]")).plusYears(count+1) );
//				allocationEntity.setAllocationYear(allocationEntity.getAllocationDate().getYear());
				allocationEntity.setGrantId(Long.parseLong(map.get("grantId").toString()));
				allocationEntity.setCreatedDate(LocalDate.now());
				
				list.add(allocationEntity);
			}
			allocationRepository.saveAll(list);
			
			longs.add(Long.parseLong( map.get("grantId").toString()));
			count++;
		}
		System.out.println(longs);
//		processUpdateAllocationStatus(longs);
		
		
    return new String("pending allocationstatus are getted succesfully");	
	}
	
	@Override
	public void processUpdateAllocationStatus(List<Long> ids) {
	
		String endpoint = "gs/approveOrAccept";
//		 UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath(endpoint).queryParam("grantId", ids).queryParam("operationType", "allocated");
        
         
		webClient.post().uri( uriBuilder ->uriBuilder
				.scheme("http")
				.host("localhost")
				.port(8082).path(endpoint)
				.queryParam("grantId", ids)
				.queryParam("operationType","allocated")
				.build()).retrieve()
		.bodyToMono(String.class)
		.block();
		
		       
	}
	
	@KafkaListener(topics= {"String"}, groupId="allocation")
	public void receiveData(String data){
		System.out.println(data);
	}
	
}
